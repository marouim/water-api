from flask import Flask, jsonify, request, Response
from pymongo import MongoClient, DESCENDING
import json

app = Flask(__name__)

@app.route("/")
def main():
  return jsonify({"home": "ok"})

@app.route("/status")
def status():
  return jsonify({"status": "ok"})

@app.route("/demo")
def demo():
  return jsonify({"status": "demo"})

@app.route("/newapp")
def newapp():
  return jsonify({"status": "Ma nouvelle application moderne Python Flask"})

app.run(host="0.0.0.0", port=8080)

